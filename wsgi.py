import os

from flask import request
from flask import Flask, render_template, abort, Response, redirect, url_for, session, flash
import mysql.connector
from flask_wtf import FlaskForm
from wtforms.fields import core, html5, simple, SelectField
from wtforms import Form, validators, widgets, ValidationError
import math
from functools import wraps
from datetime import datetime

application = Flask(__name__)
app = application
app.config['SECRET_KEY'] = 'test'


def validate_name(form, field):
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute('select count(id) from user where name=%s;', (field.data,))
    row = int(cursor.fetchone()[0])
    if row > 0:
        raise ValidationError('an account already exists with the name <strong>{}</strong>'.format(field.data))


def validate_email(form, field):
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute('select count(id) from user where email=%s;', (field.data,))
    row = int(cursor.fetchone()[0])
    if row > 0:
        raise ValidationError('an account already exists with the email <strong>{}</strong>'.format(field.data))


class RegisterForm(FlaskForm):
    name = simple.StringField(
        label="Your name",
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your name"),
            validate_name
        ],
        render_kw={"class": "form-control"}
    )

    email = html5.EmailField(
        label="Email",
        widget=widgets.TextInput(input_type='email'),
        validators=[
            validators.DataRequired(message="Enter your email"),
            validators.Email(message="Enter a valid email address"),
            validate_email
        ],
        render_kw={"class": "form-control"}
    )

    password = simple.PasswordField(
        label="Password",
        widget=widgets.PasswordInput(hide_value=False),
        validators=[
            validators.DataRequired(message="Enter your password"),
            validators.length(min=6, message="Passwords must be at least 6 characters.")
        ],
        render_kw={"class": "form-control"}
    )

    password_confirm = simple.PasswordField(
        label="Re-enter password",
        widget=widgets.PasswordInput(hide_value=False),
        validators=[
            validators.DataRequired(message="Type your password again"),
            validators.equal_to("password", message="Passwords must match")
        ],
        render_kw={"class": "form-control"}
    )


class LoginForm(FlaskForm):
    name = simple.StringField(
        label="Your name",
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your name"),
        ],
        render_kw={"class": "form-control"}
    )

    password = simple.PasswordField(
        label="Password",
        widget=widgets.PasswordInput(hide_value=False),
        validators=[
            validators.DataRequired(message="Enter your password")
        ],
        render_kw={"class": "form-control"}
    )

class AddressForm(FlaskForm):
    full_name = simple.StringField(
        label='Full name:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your full name")
        ],
        render_kw={"class": "form-control"}
    )

    address_line_1 = simple.StringField(
        label='Address line 1:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Street address, P.O. box, company name, c/o ")
        ],
        render_kw={"class": "form-control"}
    )

    address_line_2 = simple.StringField(
        label='Address line 2:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Apartment, suite, unit, building, floor, etc. ")
        ],
        render_kw={"class": "form-control"}
    )

    city = simple.StringField(
        label='City:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your city")
        ],
        render_kw={"class": "form-control"}
    )

    region = simple.StringField(
        label='State/Province/Region:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your city")
        ],
        render_kw={"class": "form-control"}
    )

    zip = simple.StringField(
        label='Zip:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your zip")
        ],
        render_kw={"class": "form-control"}
    )

    phone_number = simple.StringField(
        label='Phone number:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your phone number")
        ],
        render_kw={"class": "form-control"}
    )



class CheckoutForm(FlaskForm):
    addressid = simple.HiddenField(
        validators=[
             validators.DataRequired(message="Please select an address.")
        ],
    )

    name_on_card = simple.StringField(
        label='Name on card:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your name on card")
        ],
        render_kw={"class": "form-control"}
    )

    card_number = simple.StringField(
        label='Card number:',
        widget=widgets.TextInput(),
        validators=[
            validators.DataRequired(message="Enter your card number"),
            validators.Regexp(regex=r'[0-9]{13,19}', message="Card number must be between 13 and 19 digits long, and can only contain numbers")
        ],
        render_kw={"class": "form-control"}
    )

    expiration_date_month = SelectField(
        widget=widgets.Select(),
        choices=[(str(item), str(item)) for item in range(1, 13)],
        render_kw={"class": "form-control"}
    )

    expiration_date_year = SelectField(
        widget=widgets.Select(),
        choices=[(str(item), str(item)) for item in range(2020, 2041)],
        render_kw={"class": "form-control"}
    )


def get_db_creds():
    db = 'sampledb'
    username = 'root'
    password = 'wDxOautMbNLAOEmj'
    hostname = os.environ.get("MYSQL_SERVICE_HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def get_cnx():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        return cnx
    except Exception as exp:
        print(exp)
        raise

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('id') is None:
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

@app.route('/')
def home():
    q = request.args.get('q', '')
    page = request.args.get('page', 1)
    size = 12


    try:
        page = int(page)
    except ValueError:
        page = 1

    if q:
        q1 = '%{}%'.format(q)
    else:
        q1 = '%%'

    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute('select id,name,price,cover from goods where name like %s limit %s,%s', (q1, (page - 1) * size, size))
    goods_list = []
    for row in cursor:
        goods_list.append({
            'id': row[0],
            'name': row[1],
            'price': row[2],
            'cover': row[3]
        })
    cursor.execute('select count(id) from goods where name like %s', (q1,))
    row = cursor.fetchone()
    total_goods = int(row[0])
    total_page = math.ceil(total_goods / size)
    return render_template('home.html', goods_list=goods_list, q=q, total_goods=total_goods, total_page=total_page, current_page=page)


@app.route('/goods/<int:goods_id>')
def show_goods(goods_id):
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute('select id,name,price,cover,detail from goods where id = %s', (goods_id,))
    row = cursor.fetchone()
    if not row:
        response = Response('Sorry, we couldn\'t find that page')
        response.status_code = 404
        return abort(response)
    goods = {
        'id': row[0],
        'name': row[1],
        'price': row[2],
        'cover': row[3],
        'detail': row[4]
    }
    reviews = []
    cursor.execute('''select review.id, review.content, review.create_time, user.name 
                    from review left join user on review.userid = user.id 
                    where review.goodsid = %s 
                    order by review.create_time desc;''', (goods_id,))
    for row in cursor:
        reviews.append({
            'id': row[0],
            'content': row[1],
            'create_time': row[2],
            'user': row[3]
        })
    return render_template('goods.html', goods=goods, reviews=reviews)


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    register_success = False
    if form.validate_on_submit():
        cnx = get_cnx()
        cursor = cnx.cursor()
        cursor.execute('insert into user(name, email, password) values(%s, %s, %s)',
                       (form.name.data, form.email.data, form.password.data))
        cnx.commit()
        register_success = True
    return render_template("register.html", form=form, register_success=register_success)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    error = ''
    if form.validate_on_submit():
        cnx = get_cnx()
        cursor = cnx.cursor()
        cursor.execute('select id, name, email from user where name = %s and password = %s',
                       (form.name.data, form.password.data))
        row = cursor.fetchone()
        if row:
            session['id'] = row[0]
            session['name'] = row[1]
            session['email'] = row[2]
            next_url = request.args.get('next')
            if next_url:
                return redirect(next_url)
            else:
                return redirect(url_for('home'))
        else:
            error = 'Your name or password is incorrect'
    return render_template("login.html", form=form, error=error)


@app.route('/logout', methods=['GET'])
def logout():
    session.clear();
    return  redirect(url_for('home'))


@app.route('/add_goods', methods=['GET', 'POST'])
def add_goods():
    cart = session.get('cart', [])
    goods_id = int(request.args.get('goods_id'))
    goods = None
    for item in cart:
        print(item.get('id'), goods_id)
        if item.get('id') == goods_id:
            goods = item
            break
    if not goods:
        cnx = get_cnx()
        cursor = cnx.cursor()
        cursor.execute('select id,name,price,cover from goods where id=%s', (goods_id,))
        row = cursor.fetchone()
        if not row:
            return abort(404)
        goods = {
            'id': row[0],
            'name': row[1],
            'price': float(row[2]),
            'cover': row[3],
            'quantity': 0
        }
        print(goods)
        cart.append(goods)
    print(cart)
    if request.method == 'POST':
        quantity = goods.get('quantity')
        quantity += 1
        goods['quantity'] = quantity
        session['cart'] = cart
        return redirect(url_for('add_goods', goods_id=goods_id))
    return render_template('add_goods.html', goods=goods)

@app.route('/delete_goods', methods=['POST'])
def delete_goods():
    cart = session.get('cart', [])
    goods_id = int(request.form.get('goods_id'))
    goods = None
    index = 0
    for item in cart:
        if item.get('id') == goods_id:
            goods = item
            del cart[index]
            break
        index += 1
    flash('Delete goods successful.')
    return redirect(url_for('cart'))


@app.route('/cart', methods=['GET'])
def cart():
    cart = session.get('cart', [])
    total_quantity = 0
    total_price = 0
    total_goods = len(cart)
    for goods in cart:
        total_price += goods.get('price') * goods.get('quantity')
        total_quantity += goods.get('quantity')
    total_price = round(total_price, 2)
    return render_template('cart.html', cart=cart, total_price=total_price, total_quantity=total_quantity, total_goods=total_goods)

@app.route('/checkout', methods=['GET','POST'])
@login_required
def checkout():
    cart = session.get('cart', [])
    total_quantity = 0
    total_price = 0
    total_goods = len(cart)
    address_list = []
    for goods in cart:
        total_price += goods.get('price') * goods.get('quantity')
        total_quantity += goods.get('quantity')
    total_price = round(total_price, 2)
    form = None
    if len(cart) > 0:
        userid = session.get('id')
        form = CheckoutForm()
        cnx = get_cnx()
        cursor = cnx.cursor()
        cursor.execute(
            'select id,full_name, address_line_1, address_line_2, city, region, zip, phone_number from address where userid=%s',
            (userid,))
        address_list = []
        for row in cursor:
            address_list.append({
                'id': row[0],
                'full_name': row[1],
                'address_line_1': row[2],
                'address_line_2': row[3],
                'city': row[4],
                'region': row[5],
                'zip': row[6],
                'phone_number': row[7],
            })
        if form.validate_on_submit():
            cursor = cnx.cursor()
            data = form.data
            data['userid'] = userid
            data['create_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            data['total_price'] = total_price
            for address in address_list:
                if data['addressid'] == address['id']:
                    break
            data['full_name'] = address.get('full_name')
            data['address_line_1'] = address.get('address_line_1')
            data['address_line_2'] = address.get('address_line_2')
            data['city'] = address.get('city')
            data['region'] = address.get('region')
            data['zip'] = address.get('zip')
            data['phone_number'] = address.get('phone_number')
            sql = '''
                    insert into orders(userid, name_on_card, card_number, expiration_date_month, expiration_date_year, create_date, total_price,
                    full_name, address_line_1, address_line_2, city, region, zip, phone_number) 
                    values(%(userid)s, %(name_on_card)s, %(card_number)s,%(expiration_date_month)s, %(expiration_date_year)s, %(create_date)s, %(total_price)s,
                    %(full_name)s, %(address_line_1)s, %(address_line_2)s, %(city)s, %(region)s, %(zip)s, %(phone_number)s);
                '''
            cursor.execute(sql, data)
            orderid = cursor.lastrowid
            cnx.commit()
            print(orderid)
            for goods in cart:
                cursor.execute('insert into orders_goods(orderid,goodsid,quantity) values(%s,%s,%s)', (orderid, goods.get('id'), goods.get('quantity')))
            cnx.commit()
            session['cart'] = []
            return redirect('checkout_result')
    return render_template('checkout.html', form=form, total_goods=total_goods, total_price=total_price, cart=cart, address_list=address_list)

@app.route('/checkout_result', methods=['GET'])
@login_required
def checkout_result():
    return render_template('checkout_result.html')

@app.route('/account', methods=['GET'])
@login_required
def account():
    return render_template('account.html')

@app.route('/address_list', methods=['GET'])
@login_required
def address_list():
    userid = session.get('id')
    print(userid)
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute('select id,full_name, address_line_1, address_line_2, city, region, zip, phone_number from address where userid=%s', (userid,))
    address_list = []
    for row in cursor:
        address_list.append({
            'id': row[0],
            'full_name': row[1],
            'address_line_1': row[2],
            'address_line_2': row[3],
            'city': row[4],
            'region': row[5],
            'zip': row[6],
            'phone_number': row[7],
        })
    return render_template('address_list.html', address_list=address_list)

@app.route('/address/<address_id>', methods=['GET','POST'])
@app.route('/address', methods=['GET','POST'])
@login_required
def address_form(address_id=None):
    cnx = get_cnx()
    is_new = True
    form = AddressForm()
    if address_id:
        cursor = cnx.cursor()
        cursor.execute('select full_name, address_line_1, address_line_2, city, region, zip, phone_number from address where id=%s', (address_id,))
        row = cursor.fetchone()
        if row is None:
            return abort(404)
        is_new = False
        form.full_name.data = row[0]
        form.address_line_1.data = row[1]
        form.address_line_2.data = row[2]
        form.city.data = row[3]
        form.region.data = row[4]
        form.zip.data = row[5]
        form.phone_number.data = row[6]

    if form.validate_on_submit():
        data = form.data
        cursor = cnx.cursor()
        if is_new:
            data['userid'] = session.get('id')
            sql = '''
                insert into address(userid, full_name, address_line_1, address_line_2, city, region, zip,phone_number) 
                values(%(userid)s, %(full_name)s, %(address_line_1)s, %(address_line_2)s, %(city)s, %(region)s, %(zip)s, %(phone_number)s)
            '''
            flash('Add address successful.')
        else:
            data['id'] = address_id
            sql = '''
                update address set full_name=%(full_name)s, address_line_1=%(address_line_1)s, address_line_2=%(address_line_2)s, 
                city=%(city)s, region=%(region)s, zip=%(zip)s,phone_number=%(phone_number)s where id=%(id)s
            '''
            flash('Update address successful.')
        cursor.execute(sql, data)
        cnx.commit()
        next_url = request.args.get('next')
        if next_url:
            return redirect(next_url)
        else:
            return redirect(request.path)
    return render_template('address_form.html', form=form, is_new=is_new)


@app.route('/address_delete/<address_id>', methods=['GET'])
@login_required
def address_delete(address_id):
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute('delete from address where id=%s', (address_id,))
    cnx.commit()
    flash('Delete address successful.')
    return redirect(url_for('address_list'))


@app.route('/order_list', methods=['GET'])
@login_required
def order_list():
    userid = session.get('id')
    cnx = get_cnx()
    cursor = cnx.cursor()
    order_list = []
    orders_sql = '''
        select 
        id, name_on_card, card_number, expiration_date_month, expiration_date_year, total_price, create_date,
        full_name, address_line_1, address_line_2, city, region, zip,phone_number
        from orders where userid=%s order by create_date desc;
    '''
    cursor.execute(orders_sql, (userid,))
    order_row_list = cursor.fetchall()
    # query order
    for row in order_row_list:
        orderid = row[0]
        addressid = row[7]
        order = {
            'id': row[0],
            'name_on_card': row[1],
            'card_number': row[2],
            'expiration_date_month': row[3],
            'expiration_date_year': row[4],
            'total_price': row[5],
            'create_date': row[6],
        }

        address = {
            'full_name': row[7],
            'address_line_1': row[8],
            'address_line_2': row[9],
            'city': row[10],
            'region': row[11],
            'zip': row[12],
            'phone_number': row[13],
        }

        # query goods
        goods_sql = '''
            select 
            goods.id as goodsid,
            goods.name,
            goods.price,
            goods.cover,
            orders_goods.quantity
            from orders_goods left join goods on orders_goods.goodsid = goods.id where orderid=%s
        '''
        goods_cursor = cnx.cursor()
        goods_cursor.execute(goods_sql,(orderid,))
        goods_list = []
        for row in goods_cursor:
            goods_list.append({
                'goodsid': row[0],
                'name': row[1],
                'price': row[2],
                'cover': row[3],
                'quantity': row[4]
            })

        order['address'] = address
        order['goods_list'] = goods_list

        order_list.append(order)
        goods_cursor.close()

    print(order_list)
    return render_template('order_list.html', order_list=order_list)

@app.route('/add_review', methods=['POST'])
@login_required
def add_review():

    next_url = request.args.get('next')
    goodsid = request.form.get('goodsid')
    content = request.form.get('content')
    create_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    userid = session.get('id')
    cnx = get_cnx()
    cursor = cnx.cursor()

    try:
        goodsid = int(goodsid)
    except ValueError:
        goodsid = -1

    cursor.execute('select count(id) from goods where id = %s;', (goodsid,))
    count = int(cursor.fetchone()[0])
    if count == 0:
        flash('Goods not found.', 'text-danger')
        return redirect(next_url)

    if len(content) == 0:
        flash('review can not be empty.', 'text-danger')
        return redirect(next_url)

    cursor.execute('insert into review(goodsid, content, userid, create_time) values(%s,%s,%s,%s)', (goodsid,content,userid,create_time))
    cnx.commit()

    flash('Write review successful.', 'text-success')

    return redirect(next_url)

@app.route('/review_list', methods=['GET'])
@login_required
def review_list():

    userid = session.get('id')
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute(
        '''select review.id, review.content,review.create_time,goods.id as goodsid, goods.name as goodsname 
            from review left join goods on review.goodsid = goods.id
            where review.userid=%s 
            order by create_time desc''',
            (userid,))
    review_list = []
    for row in cursor:
        review_list.append({
            'id': row[0],
            'content': row[1],
            'create_time': row[2],
            'goodsid': row[3],
            'goodsname': row[4]
        })
    return render_template('review_list.html', review_list=review_list)


@app.route('/delete_review/<int:review_id>', methods=['GET'])
@login_required
def delete_review(review_id):
    userid = session.get('id')

    cnx = get_cnx()
    cursor = cnx.cursor()

    cursor.execute('delete from review where id = %s and userid = %s', (review_id, userid))

    cnx.commit()

    flash('delete review successful', 'has-success')

    return redirect(url_for('review_list'))



if __name__ == '__main__':
    app.debug = True
    app.run()
