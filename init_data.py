import mysql.connector
import json
import os

def get_db_creds():
    db = 'sampledb'
    username = 'root'
    password = 'wDxOautMbNLAOEmj'
    hostname = os.environ.get("MYSQL_SERVICE_HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def get_cnx():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        return cnx
    except Exception as exp:
        print(exp)
        raise

def create_goods_table():
    sql1 = '''
        DROP TABLE IF EXISTS `goods`;
    '''
    sql2 = '''
                CREATE TABLE `goods` (
                  `id` int unsigned NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) NOT NULL,
                  `cover` varchar(255) NOT NULL,
                  `price` float NOT NULL,
                  `detail` text NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
            '''
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute(sql1)
    cursor.execute(sql2)
    cnx.commit()
    cursor.close()
    cnx.close()

def create_user_table():
    sql1 = '''
        DROP TABLE IF EXISTS `user`;
    '''
    sql2 = '''
            CREATE TABLE `user` (
              `id` int unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(20) NOT NULL,
              `email` varchar(20) DEFAULT NULL,
              `password` varchar(100) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
            '''
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute(sql1)
    cursor.execute(sql2)
    cnx.commit()
    cursor.close()
    cnx.close()

def create_orders_table():
    sql1 = '''
        DROP TABLE IF EXISTS `orders`;
    '''
    sql2 = '''
            CREATE TABLE `orders` (
              `id` int unsigned NOT NULL AUTO_INCREMENT,
              `userid` int unsigned NOT NULL,
              `full_name` varchar(20) NOT NULL,
              `address_line_1` varchar(200) DEFAULT NULL,
              `address_line_2` varchar(200) DEFAULT NULL,
              `city` varchar(20) NOT NULL,
              `region` varchar(20) NOT NULL,
              `zip` varchar(10) NOT NULL,
              `phone_number` varchar(20) NOT NULL,
              `name_on_card` varchar(20) NOT NULL,
              `card_number` varchar(20) NOT NULL,
              `expiration_date_month` int NOT NULL,
              `expiration_date_year` int NOT NULL,
              `total_price` float NOT NULL,
              `create_date` datetime NOT NULL,
              PRIMARY KEY (`id`),
              KEY `userid` (`userid`),
              CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
            '''
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute(sql1)
    cursor.execute(sql2)
    cnx.commit()
    cursor.close()
    cnx.close()

def create_address_table():
    sql1 = '''
        DROP TABLE IF EXISTS `address`;
    '''
    sql2 = '''
            CREATE TABLE `address` (
              `id` int unsigned NOT NULL AUTO_INCREMENT,
              `userid` int unsigned NOT NULL,
              `full_name` varchar(20) NOT NULL,
              `address_line_1` varchar(200) DEFAULT NULL,
              `address_line_2` varchar(200) DEFAULT NULL,
              `city` varchar(20) NOT NULL,
              `region` varchar(20) NOT NULL,
              `zip` varchar(10) NOT NULL,
              `phone_number` varchar(20) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `userid` (`userid`),
              CONSTRAINT `address_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
            '''
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute(sql1)
    cursor.execute(sql2)
    cnx.commit()
    cursor.close()
    cnx.close()

def create_review_table():
    sql1 = '''
        DROP TABLE IF EXISTS `review`;
    '''
    sql2 = '''
            CREATE TABLE `review` (
              `id` int unsigned NOT NULL AUTO_INCREMENT,
              `userid` int unsigned NOT NULL,
              `goodsid` int unsigned NOT NULL,
              `content` text NOT NULL,
              `create_time` datetime NOT NULL,
              PRIMARY KEY (`id`),
              KEY `userid` (`userid`),
              KEY `goodsid` (`goodsid`),
              CONSTRAINT `review_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`),
              CONSTRAINT `review_ibfk_2` FOREIGN KEY (`goodsid`) REFERENCES `goods` (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
            '''
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute(sql1)
    cursor.execute(sql2)
    cnx.commit()
    cursor.close()
    cnx.close()

def create_orders_goods_table():
    sql1 = '''
        DROP TABLE IF EXISTS `orders_goods`;
    '''
    sql2 = '''
            CREATE TABLE `orders_goods` (
              `id` int unsigned NOT NULL AUTO_INCREMENT,
              `orderid` int unsigned NOT NULL,
              `goodsid` int unsigned NOT NULL,
              `quantity` int NOT NULL,
              PRIMARY KEY (`id`),
              KEY `orderid` (`orderid`),
              KEY `goodsid` (`goodsid`),
              CONSTRAINT `orders_goods_ibfk_1` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`),
              CONSTRAINT `orders_goods_ibfk_2` FOREIGN KEY (`goodsid`) REFERENCES `goods` (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
            '''
    cnx = get_cnx()
    cursor = cnx.cursor()
    cursor.execute(sql1)
    cursor.execute(sql2)
    cnx.commit()
    cursor.close()
    cnx.close()

def init_goods():
    with open('./data.json', encoding='utf-8') as f:
        goods_list = json.load(f)
        cnx = get_cnx()
        cursor = cnx.cursor()
        for goods in goods_list:
            name = goods.get('name')
            cover = goods.get('cover')
            price = goods.get('price')
            detail = goods.get('detail')
            cursor.execute('insert into goods(name, cover, price, detail) values(%s, %s, %s, %s);', (name, cover, price, detail))
        cnx.commit()

if __name__ == '__main__':
    create_goods_table()
    create_user_table()
    create_orders_table()
    create_orders_goods_table()
    create_address_table()
    create_review_table()
    init_goods()